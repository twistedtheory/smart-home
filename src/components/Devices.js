import React, {Component} from 'react';
import _, {debounce} from 'lodash';
import '../styles/App.css';
import config from '../common/config.js';
import Clock from 'react-live-clock';

const ApiHelper = require('../common/ApiHelper.js');
const {winkAuthRequestBody, tplinkAuthRequestBody} = config;
const {winkApiBaseUrl, winkAuthBaseUrl, tplinkAuthBaseUrl, tplinkApiBaseUrl} = ApiHelper;

class Devices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            winkAccessToken: '',
            tplinkAccessToken: ''
        };
    }

    updateBulb(bulb, requestBody, key) {
        ApiHelper
            .put(`${winkApiBaseUrl}/light_bulbs/${bulb.light_bulb_id}/desired_state`, this.state.winkAccessToken, requestBody)
            .catch(() => this.setWinkDevicesList(bulb, key));
    }

    componentWillMount() {
        this.loginToTpLink();
        this.loginToWink();
        this.updateBulb = debounce(this.updateBulb, 500);
    }

    handleGetWinkDevices(response) {
        response
            .data
            .splice(0, 1);
        this.setState({winkDevices: response.data});
    }

    handleGetTplinkDevices(response) {
        this.setState({tplinkDevices: response.result.deviceList});
    }

    setWinkAccessToken(response) {
        this.setState({winkAccessToken: response.access_token});
    }

    setTplinkAccessToken(response) {
        this.setState({tplinkAccessToken: response.result.token});
    }

    getWinkDevices() {
        if (this.state.winkAccessToken) {
            ApiHelper
                .get(`${winkApiBaseUrl}/users/me/wink_devices`, this.state.winkAccessToken)
                .then(response => this.handleGetWinkDevices(response));
        }
    }

    getTpLinkDevices() {
        const requestBody = {
            method: 'getDeviceList'
        };
        ApiHelper
            .post(`${tplinkApiBaseUrl}?token=${this.state.tplinkAccessToken}`, requestBody)
            .then(response => this.handleGetTplinkDevices(response));
    }

    loginToWink() {
        ApiHelper.makeRequest(winkAuthBaseUrl, winkAuthRequestBody, {
            'Content-type': 'application/json; charset=UTF-8'
        }, 'post')
            .then(response => this.setWinkAccessToken(response))
            .then(() => this.getWinkDevices())
            .catch(response => {
                console.error('could not log in', response);
            });
    }

    loginToTpLink() {
        ApiHelper.makeRequest(tplinkAuthBaseUrl, tplinkAuthRequestBody, {
            'Content-type': 'application/json; charset=UTF-8'
        }, 'post')
            .then(response => this.setTplinkAccessToken(response))
            .then(() => this.getTpLinkDevices())
            .catch(response => {
                console.error('could not log in', response);
            });
    }

    togglePower(e, bulb, key) {
        e.preventDefault();
        let requestBody = {
            desired_state: {
                powered: !bulb.desired_state.powered,
                brightness: bulb.desired_state.brightness
            }
        };
        this.setWinkDevicesList(requestBody, key);
        this.updateBulb(bulb, requestBody, key);
    }

    toggleTplinkDevicePower(e, device, key) {
        e.preventDefault();
        const status = device.status === 1
            ? 0
            : 1;
        let requestBody = {
            method: 'passthrough',
            params: {
                deviceId: device.deviceId,
                requestData: '{"system":{"set_relay_state":{"state":' + status + '}}}'
            }
        };
        this.setTplinkDevicesList(requestBody, key, status);
        this.updateTplinkDevice(device, requestBody, key);
    }

    adjustBrightness(e, bulb, key) {
        let brightness = parseFloat(_.get(e, 'target.value', 0), 10);
        let requestBody = {
            desired_state: {
                powered: brightness !== 0,
                brightness: _.get(e, 'target.value', 0)
            }
        };
        this.setWinkDevicesList(requestBody, key);
        this.updateBulb(bulb, requestBody, key);
    }

    updateTplinkDevice(device, requestBody, key) {
        ApiHelper
            .post(`${tplinkApiBaseUrl}?token=${this.state.tplinkAccessToken}`, requestBody)
            .catch(() => this.setTplinkDevicesList(device, key));
    }

    setWinkDevicesList(bulb, key) {
        const bulbs = this
            .state
            .winkDevices
            .slice();
        bulbs[key].desired_state = bulb.desired_state;
        this.setState({winkDevices: bulbs});
    }

    setTplinkDevicesList(device, key, status) {
        const devices = this
            .state
            .tplinkDevices
            .slice();
        devices[key].status = status;
        this.setState({tplinkDevices: devices});
    }

    reloadDevices() {
        this.getWinkDevices();
        this.getTpLinkDevices();
    }

    render() {
        return (
            <div className="Devices">
                <div>
                    {!this.state.winkAccessToken && <span>Logging in...</span>}
                </div>
                <div
                    className='clock'
                    style={{
                    display: 'inline-block'
                }}>
                    <span className='hour'>
                        <Clock format={'hh'} ticking={true} timezone={'US/Eastern'}/>
                    </span>
                    <span className="colon">:</span>
                    <span className='minute'>
                        <Clock format={'mm'} ticking={true} timezone={'US/Eastern'}/>
                    </span>
                    <span className="colon">:</span>
                    <span className='second'>
                        <Clock format={'ss'} ticking={true} timezone={'US/Eastern'}/>
                    </span>
                    <span className='period'>
                        <Clock format={'A'} ticking={true} timezone={'US/Eastern'}/>
                    </span>
                </div>
                <div
                    style={{
                    display: 'inline-block',
                    float: 'right',
                    margin: '5px'
                }}>
                    <a
                        className="button icon"
                        style={{
                        backgroundColor: '#b5c8cf',
                        backgroundImage: `url(/images/sync.png)`,
                        width: '60px',
                        height: '60px',
                        padding: '10px'
                    }}
                        onClick={() => this.reloadDevices()}/>
                </div>
                <iframe
                    src='//forecast.io/embed/#lat=38.910946&lon=-77.221609&name=Vienna, VA&color=#ffc756'
                    frameBorder='0px'
                    height='245'
                    width='100%'/>
                <div className='controls-wrapper'>
                    <ul className="w3-ul-2">
                        {this.state.tplinkDevices && this
                            .state
                            .tplinkDevices
                            .map((value, key) => (
                                <li
                                    key={key}
                                    style={{
                                    margin: '5px',
                                    lineHeight: '2.5'
                                }}
                                    className="list-items device-item">
                                    {value.deviceId && <div>
                                        <a
                                            className={`button icon`}
                                            href="#"
                                            title={`Toggle ${value.alias}`}
                                            onClick={e => this.toggleTplinkDevicePower(e, value, key)}
                                            style={{
                                            backgroundColor: _.get(value, 'status') === 1
                                                ? '#ffc756'
                                                : '#b5c8cf',
                                            backgroundImage: `url(/images/${_.kebabCase(value.alias)}.png)`
                                        }}/>
                                    </div>}
                                </li>
                            ))}
                        {this.state.winkDevices && this
                            .state
                            .winkDevices
                            .map((value, key) => (value.light_bulb_id
                                ? <li
                                        key={key}
                                        style={{
                                        margin: '5px',
                                        lineHeight: '2.5'
                                    }}
                                        className="list-items device-item-with-range">
                                        <div>
                                            <a
                                                className={`button icon ${_.kebabCase(value.name)}`}
                                                href="#"
                                                title={`Toggle ${value.name}`}
                                                onClick={e => this.togglePower(e, value, key)}
                                                style={{
                                                backgroundColor: _.get(value, 'desired_state.powered')
                                                    ? '#ffc756'
                                                    : '#b5c8cf',
                                                backgroundImage: `url(/images/${_.kebabCase(value.name)}.png)`
                                            }}/>
                                            <input
                                                id={key}
                                                type="range"
                                                min="0"
                                                max="1"
                                                value={_.get(value, 'desired_state.brightness')}
                                                onChange={e => this.adjustBrightness(e, value, key)}
                                                step=".1"/>
                                        </div>
                                    </li>
                                : value.upc_code === 'wink_dw_sensor' && <li
                                    key={key}
                                    style={{
                                    margin: '5px',
                                    lineHeight: '2.5'
                                }}
                                    className="list-items device-item">
                                    <a
                                        className={`icon-no-press icon ${_.kebabCase(value.name)}`}
                                        style={{
                                        backgroundColor: _.get(value, 'last_reading.opened')
                                            ? '#ffc756'
                                            : '#b5c8cf',
                                        backgroundImage: _.get(value, 'last_reading.opened')
                                            ? `url(/images/door-open.png)`
                                            : `url(/images/door-closed.png)`
                                    }}/>
                                </li>))}
                    </ul>
                </div>
            </div>
        );
    }
}

export default Devices;
