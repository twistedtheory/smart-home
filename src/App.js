import React from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Devices from './components/Devices';
import './styles/App.css';

const Home = () => (
  <div>
    <h2>Home</h2>
  </div>
);

const App = () => (
  <div className="App">
    <div className="App-intro">
      <Router>
        <div>
          <Route exact path="/smart-home" component={Home}/>
          <Route path="/smart-home/devices" component={Devices}/>
          <hr/>
          <ul className="w3-ul">
            <li>
              <Link to="/smart-home/devices">Devices</Link>
            </li>
          </ul>
        </div>
      </Router>
    </div>
  </div>
);
export default App;
