import _ from 'lodash';

const APPLICATION_JSON = 'application/json';
const WINK_AUTH_BASE_URL = 'https://api.wink.com/oauth2/token';
const WINK_API_BASE_URL = 'https://winkapi.quirky.com';
const TPLINK_AUTH_BASE_URL = 'https://wap.tplinkcloud.com';
const TPLINK_API_BASE_URL = 'https://use1-wap.tplinkcloud.com';

function makeRequest(url, body, headers, method = 'get', accept = 'application/json') {
  return fetch(url, {
    accept: accept,
    credentials: 'include',
    headers: headers,
    method: method,
    body: body
      ? JSON.stringify(body)
      : null
    })
    .then(promise => processStatus(promise))
    .then(data => convertToJson(data))
    .catch(response => {
      console.error('something bad happened', response, url);
    });
}
function get(url, accessToken) {
  let authHeaders = {
    'Content-type': APPLICATION_JSON,
    Authorization: `Bearer ${accessToken}`
  };
  let ajaxConfig = {
    accept: APPLICATION_JSON,
    credentials: 'include',
    headers: authHeaders,
    method: 'get'
  };
  return fetch(url, ajaxConfig)
    .then(promise => processStatus(promise))
    .then(data => convertToJson(data))
    .catch(data => console.error(data));
}
function put(url, accessToken, requestBody) {
  const authHeaders = {
    'Content-type': APPLICATION_JSON,
    Authorization: `Bearer ${accessToken}`
  };
  return makeRequest(url, requestBody, authHeaders, 'put');
}

function post(url, requestBody) {
  const authHeaders = {
    'Content-type': APPLICATION_JSON
  };
  return makeRequest(url, requestBody, authHeaders, 'post');
}

function convertToJson(data) {
  // data is CORS object
  return data.json();
}

function processStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response);
  } else {
    return Promise.reject(new Error(_.get(response, 'data.statusText', JSON.stringify(response))));
  }
}

exports.makeRequest = makeRequest;
exports.get = get;
exports.put = put;
exports.post = post;
exports.winkAuthBaseUrl = WINK_AUTH_BASE_URL;
exports.winkApiBaseUrl = WINK_API_BASE_URL;
exports.tplinkAuthBaseUrl = TPLINK_AUTH_BASE_URL;
exports.tplinkApiBaseUrl = TPLINK_API_BASE_URL;
