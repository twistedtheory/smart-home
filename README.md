## Installation
- `yarn install`
- `yarn build`
- `yarn global add serve`
- `serve -s build` or copy `build/` folder contents to your default webserver and deploy. Be sure to modify package.json file with appropriatge `homepage` values

## RPi Kiosk Mode
If you wish to launch this app on kiosk mode on raspberry pi at startup, then copy over the following lines to ~/.config/lxsession/LXDE-pi/autostart
If you're deploying the app through a webserver as mentioned above, then please be sure to modify link on line {} accordingly otherwise make sure you are running `yarn start` on the project directory
```
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
@xscreensaver -no-splash
@point-rpi

@xscreensaver -no-splash  # comment this line out to disable screensaver
@xset s off
@xset -dpms
@xset s noblank
@unclutter -idle 0
@chromium-browser --kiosk https://localhost:3000/ --overscroll-history-navigation=0
``` 
## RPi Screen Brightness
If you are using RPi 7" Touchscreen Display and want to control the brightness of screen based on surrounding light, 
Please look into: https://bitbucket.org/twistedtheory/auto-adjust-rpi-screen-brighness.git
